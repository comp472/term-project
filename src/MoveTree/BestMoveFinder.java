package MoveTree;

import java.util.ArrayList;

import Board.Board;
import Player.*;
import Heuristic.Heuristic;

public class BestMoveFinder 
{
	private Board originalBoard;
	private Board copyBoard;
	private WhitePlayer whitePlayer;
	private BlackPlayer blackPlayer;
	
	//Go x levels down
	private int depthSearch = 3;
	
	public BestMoveFinder(WhitePlayer wp, BlackPlayer bp)
	{
		this.whitePlayer = wp;
		this.blackPlayer = bp;
	}
	
	public MoveNode getNextMove(Board board, Player player)
	{
		this.originalBoard = board;
		this.copyBoard = board.getCopy();
		return getNextMove(player, depthSearch, copyBoard).node;
	}
	
	class ScoreMove
	{
		int score;
		MoveNode node;
	}
	
	private ScoreMove getNextMove(Player p, int depth, Board currentBoard)
	{
		// get all possible moves
		ArrayList<MoveNode> showMeYourMoves = p.getAllPossibleMoves(currentBoard);

		int currentScore;
		int bestScore;
		MoveNode bestNode = null;

		// end of recursion
		if (showMeYourMoves.isEmpty() || depth == 0)
		{
			bestScore = heuristic(currentBoard);
		}
		else
		{
			if (p instanceof WhitePlayer)
			{
				// max player
				bestScore = Integer.MIN_VALUE;
			}
			else
			{
				// min player
				bestScore = Integer.MAX_VALUE;
			}
			
			for(int i = 0; i < showMeYourMoves.size(); i++)
			{
				Board b = currentBoard.getCopy();
				
				// play the current move
				p.play(b, showMeYourMoves.get(i).getFirstTokenPosition(), showMeYourMoves.get(i).getSecondTokenPosition());

				if(p instanceof WhitePlayer)
				{
					ScoreMove a = getNextMove(blackPlayer, depth - 1, b);
					currentScore = a.score;
					
					if (currentScore > bestScore)
					{
						bestNode = showMeYourMoves.get(i);
						bestScore = currentScore;
					}
				}
				else
				{
					ScoreMove a = getNextMove(whitePlayer, depth - 1, b);
					currentScore = a.score;
					
					if(currentScore < bestScore)
					{
						bestNode = showMeYourMoves.get(i);
						bestScore = currentScore;
					}
				}
				// undo last move
				b = currentBoard.getCopy();
			}
		}
		
		// return best move and score of that move
		ScoreMove sm = new ScoreMove();
		sm.node = bestNode;
		sm.score = bestScore;
		
		return sm;
	}

	
	private int heuristic(Board b)
	{
		return Heuristic.getInstance().getHeuristicForBoard(b);
	}
}
