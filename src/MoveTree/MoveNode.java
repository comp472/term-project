package MoveTree;

import java.util.ArrayList;

public class MoveNode 
{
	private String firstToken;
	private String secondToken;
	
	public ArrayList<MoveNode> children;
	
	public MoveNode(String firstToken, String secondToken)
	{
		this.children = new ArrayList<MoveNode>();
		this.firstToken = firstToken;
		this.secondToken = secondToken;
	}
	
	public String getFirstTokenPosition() 
	{
		return firstToken;
	}
	
	public void setFirstToken(String firstToken) 
	{
		this.firstToken = firstToken;
	}
	
	public String getSecondTokenPosition() 
	{
		return secondToken;
	}
	
	public void setSecondToken(String secondToken) 
	{
		this.secondToken = secondToken;
	}
	
}
