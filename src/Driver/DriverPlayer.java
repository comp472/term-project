package Driver;

import Board.*;
import Player.*;
import Util.*;

import java.util.Scanner;

public class DriverPlayer 
{
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		MessagePrinter message = new MessagePrinter();
		VariableSetter setter = new VariableSetter();
		
		message.printWelcomeMessage();
		
		String playerOneName = setter.getPlayerName(scanner, message, 1);
		String playerTwoName = setter.getPlayerName(scanner, message, 2);
		WhitePlayer wp = new WhitePlayer(playerOneName);
		BlackPlayer bp = new BlackPlayer(playerTwoName);
		message.printPlayerRoles(playerOneName, playerTwoName);
		
		Board board = new Board(8);
		
		boolean isGameOver = false;
		
		while(!isGameOver)
		{
			isGameOver = setter.play(scanner, message, board, wp, bp.getName());
			if(isGameOver)
			{
				break;
			}
			isGameOver = setter.play(scanner, message, board, bp, wp.getName());
		}
	}
}
