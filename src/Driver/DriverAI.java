package Driver;

import Board.*;
import MoveTree.BestMoveFinder;
import MoveTree.MoveNode;
import Player.*;
import Util.*;

import java.util.Calendar;
import java.util.Scanner;

public class DriverAI 
{
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		MessagePrinter message = new MessagePrinter();
		VariableSetter setter = new VariableSetter();
		
		message.printWelcomeMessage();
		
		String playerOneName = setter.getPlayerName(scanner, message, 1);
		String playerTwoName = setter.getPlayerName(scanner, message, 2);
		WhitePlayer wp = new WhitePlayer(playerOneName);
		BlackPlayer bp = new BlackPlayer(playerTwoName);
		message.printPlayerRoles(playerOneName, playerTwoName);
		
		Board board = new Board(8);
		
		boolean isGameOver = false;
		
		BestMoveFinder bmf = new BestMoveFinder(wp, bp);
		
		while(!isGameOver)
		{
			long preTime = Calendar.getInstance().getTimeInMillis();
			MoveNode nextMoveWp = bmf.getNextMove(board, wp);
			long postTime = Calendar.getInstance().getTimeInMillis();
			
			System.out.println("Calculated in: " + (postTime - preTime)/1000.0 + "seconds");
			
			isGameOver = setter.playAI(nextMoveWp, message, board, wp, bp.getName());
			if(isGameOver)
			{
				break;
			}
			
			long preTime2 = Calendar.getInstance().getTimeInMillis();
			MoveNode nextMoveBp = bmf.getNextMove(board, bp);
			long postTime2 = Calendar.getInstance().getTimeInMillis();
			
			System.out.println("Calculated in: " + (postTime2 - preTime)/1000.0 + "seconds");
			isGameOver = setter.playAI(nextMoveBp, message, board, bp, wp.getName());
		}
	}
}
