package Heuristic;

import Board.Board;
import Board.Tile;
import Player.BlackPlayer;
import Player.WhitePlayer;

public class Heuristic 
{
	private static Heuristic instance;
	
	public static Heuristic getInstance()
	{
		if(instance == null)
		{
			instance = new Heuristic();
		}
		
		return instance;
	}
	
	/*
	 * 1) Calculate the number of moves available for X - sent as a parameter
	 * 2) Calculate the number of tiles that are guaranteed to be X
	 * 3) Calculate the number of tiles that are either going to be X or empty
	 */
	public int getHeuristicForBoard(Board b)
	{
		int heuristicValue = 0;
		Tile[][] board = b.getBoard();
		
		for(int column = 0; column < board[0].length; column++)
		{
			boolean availableTileFound = false;
			for(int row = 0; row < board.length; row++)
			{
				if(availableTileFound && board[row][column].getTokenOnTile() == null)
				{
					//New Move Found
					++heuristicValue;
					
					//Check if guaranteed WhitePlayer Token
					if(isLessThanZero(column - 1))
					{	
						Tile firstRightTile = getFirstTileOnRight(board, row, column);
						Tile secondRightTile = getSecondTileOnRight(board, row, column);
						
						if(firstRightTile.getTokenOnTile() != null)
						{
							++heuristicValue;
						}
						
						if(secondRightTile.getTokenOnTile() != null)
						{
							++heuristicValue;
						}
					}
					else if(isGreaterThanBound(column + 1, board.length - 1))
					{
						Tile firstLeftTile = getFirstTileOnLeft(board, row, column);
						Tile secondLeftTile = getSecondTileOnLeft(board, row, column);
						
						if(firstLeftTile.getTokenOnTile() != null)
						{
							++heuristicValue;
						}
						
						if(secondLeftTile.getTokenOnTile() != null)
						{
							++heuristicValue;
						}
					}
					else
					{
						Tile firstRightTile = getFirstTileOnRight(board, row, column);
						Tile secondRightTile = getSecondTileOnRight(board, row, column);
						
						Tile firstLeftTile = getFirstTileOnLeft(board, row, column);
						Tile secondLeftTile = getSecondTileOnLeft(board, row, column);
						
						if(firstLeftTile.getTokenOnTile() != null && firstRightTile.getTokenOnTile() != null)
						{
							++heuristicValue;
						}
						
						if(secondLeftTile.getTokenOnTile() != null && secondRightTile.getTokenOnTile() != null)
						{
							++heuristicValue;
						}
					}
				}
				
				if(board[row][column].getTokenOnTile() == null)
				{
					availableTileFound = true;
				}
				else
				{
					availableTileFound = false;
				}
			}
		}
		
		int wpMoves = (new WhitePlayer("Temp")).getAllPossibleMoves(b).size();
		int bpMoves = (new BlackPlayer("Temp")).getAllPossibleMoves(b).size();
		
		if(wpMoves > bpMoves)
		{
			heuristicValue += (wpMoves - bpMoves);
		}
		
		return heuristicValue;
	}
	
	private boolean isLessThanZero(int x)
	{
		return x < 0;
	}
	
	private boolean isGreaterThanBound(int x, int bound)
	{
		return x > bound;
	}
	
	private Tile getFirstTileOnLeft(Tile[][] board, int row, int col)
	{
		return board[row - 1][col - 1];
	}
	
	private Tile getSecondTileOnLeft(Tile[][] board, int row, int col)
	{
		return board[row][col - 1];
	}
	
	private Tile getFirstTileOnRight(Tile[][] board, int row, int col)
	{
		return board[row - 1][col + 1];
	}
	
	private Tile getSecondTileOnRight(Tile[][] board, int row, int col)
	{
		return board[row][col + 1];
	}
	
}
