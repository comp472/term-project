package Board;

import java.util.HashMap;

import Token.Token;

public class Board 
{
	private Tile[][] board;
	private HashMap<Integer, Character> tileIntToChar;
	private HashMap<Character, Integer> tileCharToInt;

	public Board(int size)
	{
		board = new Tile[size][size];
		tileIntToChar = new HashMap<Integer, Character>();
		tileCharToInt = new HashMap<Character, Integer>();
		populateTileNames(size);
		setTileNames();
	}
	
	public Tile[][] getBoard() 
	{
		return board;
	}

	public HashMap<Integer, Character> getTileIntToChar() 
	{
		return tileIntToChar;
	}

	public HashMap<Character, Integer> getTileCharToInt() 
	{
		return tileCharToInt;
	}

	private void populateTileNames(int size) 
	{
		int valueOfA = 'A';
		for(int i = 0; i < size; i++)
		{
			tileIntToChar.put(i, (char)valueOfA);
			tileCharToInt.put((char)valueOfA, i);
			valueOfA++;
		}	
	}
	
	private void setTileNames() 
	{
		for(int row = 0; row < board.length; row++)
		{
			String rowName = tileIntToChar.get(row).toString();
			for(int column = 0; column < board[0].length; column++)
			{
				board[row][column] = new Tile(rowName + (column+1));
			}
		}
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getColumnHeader());
		for(int row = 0; row < board.length; row++)
		{
			String rowName = tileIntToChar.get(row).toString();
			sb.append(formatForGridLayout(rowName));
			for(int column = 0; column < board[0].length; column++)
			{	
				sb.append(formatForGridLayout(board[row][column].getTokenOnTileBoardIcon()));
			}
			sb.append("|\n");
		}
		
		return sb.toString();
	}
	
	private String getColumnHeader()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("  ");
		for(int i = 0; i < board[0].length; i++)
		{
			sb.append(formatForGridLayout((i+1) + ""));
		}
		sb.append("|\n");
		
		return sb.toString();
	}
	
	private String formatForGridLayout(String str)
	{
		return "|" + str;
	}
	
	public Board getCopy()
	{
		Board boardCopy = new Board(this.board.length);
		
		for(int row = 0; row < this.board.length; row++)
		{
			for(int column = 0; column < this.board[0].length; column++)
			{
				Token originalToken = this.board[row][column].getTokenOnTile();
				if(originalToken != null)
				{
					boardCopy.getBoard()[row][column].placeToken(originalToken.getCopy());
				}
			}
		}
		
		return boardCopy;
	}
}
