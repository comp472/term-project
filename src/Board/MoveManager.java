package Board;

import Player.Player;
import Token.Token;

public class MoveManager 
{
	private MoveValidator validator;
	
	public MoveManager()
	{
		validator = new MoveValidator();
	}
	
	public boolean placeTokens(Player player, Board board, String tokenOnePosition, String tokenTwoPosition)
	{
		if(!validator.areArgumentsValid(player, board, tokenOnePosition, tokenTwoPosition))
		{
			return false;
		}
		
		placeToken(player, board, tokenOnePosition);
		placeToken(player, board, tokenTwoPosition);
		
		return true;
	}
	
	private void placeToken(Player player, Board board, String position)
	{
		Token token = player.getToken();
		
		int row = board.getTileCharToInt().get(position.charAt(0));
		int column = Integer.parseInt(position.charAt(1) + "") - 1;
		
		board.getBoard()[row][column].placeToken(token);
	}
}
