package Board;

import Player.*;

public class MoveValidator 
{
	public boolean areArgumentsValid(Player player, Board board, String tokenOnePosition, String tokenTwoPosition)
	{
		if(!isArgumentValid(board,tokenOnePosition) || !isArgumentValid(board,tokenTwoPosition))
		{
			return false;
		}
		
		if(!isMoveAllowedForPlayer(player, board, tokenOnePosition, tokenTwoPosition))
		{
			return false;
		}
		
		return true;
	}
	
	private boolean isArgumentValid(Board board, String position)
	{
		if(position.length() != 2)
		{
		//	System.out.println(position + " - must be of length 2.");
			return false;
		}
		
		if(!board.getTileCharToInt().containsKey(position.charAt(0)))
		{
			System.out.println(position + " - 1st character is not a valid letter.");
			return false;
		}
		
		if(!Character.isDigit(position.charAt(1)))
		{
			System.out.println(position + " - 2nd character is not a digit.");
			return false;
		}
		
		int columnIndex = Integer.parseInt(position.charAt(1) + "");
		if(columnIndex < 0 || columnIndex > board.getBoard()[0].length)
		{
			System.out.println(position + " - 2nd digit is not a valid number.");
			return false;
		}
		
		int rowIndex = board.getTileCharToInt().get(position.charAt(0));
		if(board.getBoard()[rowIndex][columnIndex - 1].getTokenOnTile() != null)
		{
			System.out.println(position + " - is already taken by another token.");
			return false;
		}
		
		return true;
	}
	
	private boolean isMoveAllowedForPlayer(Player player, Board board, String tokenOnePosition, String tokenTwoPosition)
	{
		if(!player.isMoveAllowed(board, tokenOnePosition, tokenTwoPosition))
		{
			return false;
		}
		
		return true;
	}
}
