package Board;

import Token.Token;

public class Tile 
{
	private String tileName;
	private Token tokenOnTile;
	
	public Tile(String tileName)
	{
		this.tileName = tileName;
		this.tokenOnTile = null;
	}
	
	public String getTileName()
	{
		return tileName;
	}
	
	public void placeToken(Token token)
	{
		tokenOnTile = token;
	}
	
	public Token getTokenOnTile()
	{
		return tokenOnTile;
	}
	
	public String getTokenOnTileBoardIcon()
	{
		if(tokenOnTile == null)
		{
			return " ";
		}
		
		return tokenOnTile.getBoardIcon() + "";
	}
}
