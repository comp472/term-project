package Token;

public class Token 
{
	String tilePosition;
	char boardIcon;
	
	public String getTilePosition() 
	{
		return tilePosition;
	}
	
	public void setTilePosition(String tilePosition) 
	{
		this.tilePosition = tilePosition;
	}
	
	public char getBoardIcon() 
	{
		return boardIcon;
	}
	
	public void setBoardIcon(char boardIcon) 
	{
		this.boardIcon = boardIcon;
	}
	
	public Token getCopy()
	{
		Token tokenCopy = new Token();
		tokenCopy.setTilePosition(this.getTilePosition());
		tokenCopy.setBoardIcon(this.getBoardIcon());
		
		return tokenCopy;
	}
}
