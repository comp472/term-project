package Util;

import java.util.Scanner;

import Board.Board;
import MoveTree.MoveNode;
import Player.Player;

public class VariableSetter 
{
	public String getPlayerName(Scanner scanner, MessagePrinter message, int playerNumber)
	{
		String playerName;
		
		do
		{
			message.printEnterYourName(playerNumber);
			playerName = scanner.nextLine();
			
			if(playerName.length() < 1)
			{
				message.printInvalidName();
			}
		}
		while(playerName.length() < 1);
		
		return playerName;
	}
	
	private String getPlayerTokenPosition(Scanner scanner, MessagePrinter message, int tokenNumber)
	{
		String tokenPosition;
		do
		{
			message.printEnterYourTokenPosition(tokenNumber);
			tokenPosition = scanner.nextLine();
			
			if(tokenPosition.length() < 2)
			{
				message.printInvalidTokenPosition();
			}
		}
		while(tokenPosition.length() < 2);
		
		return tokenPosition;
	}
	
	public String getPlayerTokenPosition(Scanner scanner, MessagePrinter message, int tokenNumber, String playerName, String playerValidMove)
	{
		message.printPlayerReminder(playerName, playerValidMove);
		return getPlayerTokenPosition(scanner, message, tokenNumber);
	}
	
	public boolean play(Scanner scanner, MessagePrinter message, Board board, Player player, String opposingPlayerName)
	{
		if(player.getAllPossibleMoves(board).size() < 1)
		{
			message.printLoser(player.getName(), opposingPlayerName);
			return true;
		}
		
		boolean success = false;
		do
		{
			System.out.println(board);
			String tokenOnePositionWhitePlayer = getPlayerTokenPosition(scanner, message, 1, player.getName(), player.getValidMove());
			String tokenTwoPositionWhitePlayer = getPlayerTokenPosition(scanner, message, 2, player.getName(), player.getValidMove());
			success = player.play(board, tokenOnePositionWhitePlayer, tokenTwoPositionWhitePlayer);
			
			if(!success)
			{
				message.printInvalidTokenRules();
			}
		}
		while(!success);
		
		return false;
	}
	
	public boolean playAI(MoveNode move, MessagePrinter message, Board board, Player player, String opposingPlayerName)
	{
		if(player.getAllPossibleMoves(board).size() < 1)
		{
			message.printLoser(player.getName(), opposingPlayerName);
			return true;
		}
		
		boolean success = false;
		do
		{
			System.out.println(board);
			System.out.println("AI PLAYS: " + move.getFirstTokenPosition() + " " + move.getSecondTokenPosition());
			success = player.play(board, move.getFirstTokenPosition(), move.getSecondTokenPosition());
			
			if(!success)
			{
				message.printInvalidTokenRules();
			}
		}
		while(!success);
		
		return false;
	}
}
