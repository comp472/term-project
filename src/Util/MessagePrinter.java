package Util;

public class MessagePrinter 
{
	public void printWelcomeMessage()
	{
		System.out.println("----------------------------------------------------------");
		System.out.println("-Welcome to the game of Ganji-Oh--------------------------");
		System.out.println("-Create by Dror Ozgaon and David Siekut-------------------");
		System.out.println("----------------------------------------------------------");
		System.out.println("-Rules----------------------------------------------------");
		System.out.println("-White tokens player can only place tokens vertically-----");
		System.out.println("-Black tokens player can only place tokens horizontally---");
		System.out.println("-When entering a token position, enter values as follows--");
		System.out.println("-Example: \"A2\"- row letter and column index attached------");
		System.out.println("----------------------------------------------------------");
	}
	
	public void printEnterYourName(int playerNumber)
	{
		System.out.println("Please Enter the name for Player " + playerNumber);
	}
	
	public void printInvalidName()
	{
		System.out.println("Oops! That's an invalid name. Try again.");
	}

	public void printPlayerReminder(String playerName, String playerValidMove) 
	{
		System.out.println("It's " + playerName + "'s turn. Remember, you can only place your tokens " + playerValidMove + "!");
	}

	public void printEnterYourTokenPosition(int tokenNumber) 
	{
		System.out.println("Please Enter the position for Token " + tokenNumber);
	}

	public void printInvalidTokenPosition() 
	{
		System.out.println("Oops! That's an invalid format for the token position. Try again.");
	}
	
	public void printInvalidTokenRules() 
	{
		System.out.println("Uh-oh. Someone isn't following the rules. Try again.");
	}
	
	public void printPlayerRoles(String whitePlayer, String blackPlayer) 
	{
		System.out.println(whitePlayer + " goes first - White tokens. " + blackPlayer + " goes second.");
	}

	public void printLoser(String playerName, String opposingPlayerName) 
	{
		System.out.println("****************************************************************");
		System.out.println(playerName + " lost! He cannot place anymore tokens.");
		System.out.println(opposingPlayerName + " is the winner!");
		System.out.println("****************************************************************");
	}
}
