package Player;

import java.util.ArrayList;

import Board.Board;
import Board.Tile;
import MoveTree.MoveNode;
import Token.*;

public class BlackPlayer extends Player
{
	public BlackPlayer(String name)
	{
		super(name);
	}

	@Override
	public BlackToken getToken() 
	{
		return new BlackToken();
	}

	@Override
	public boolean isMoveAllowed(Board board, String tokenOnePosition, String tokenTwoPosition) 
	{
		int columnIndexTokenOne = Integer.parseInt(tokenOnePosition.charAt(1) + "");
		int columnIndexTokenTwo = Integer.parseInt(tokenTwoPosition.charAt(1) + "");
		
		int columnIndexDifference = Math.abs(columnIndexTokenOne - columnIndexTokenTwo);
		if(columnIndexDifference != 1 || tokenOnePosition.charAt(0) != tokenTwoPosition.charAt(0))
		{
			System.out.println("You can only place your tokens horizontally!");
			return false;
		}
		
		return true;
	}

	@Override
	public String getValidMove() 
	{
		return "horizontally";
	}

	@Override
	public ArrayList<MoveNode> getAllPossibleMoves(Board board) 
	{
		ArrayList<MoveNode> possibleMoves = new ArrayList<MoveNode>();
		Tile[][] gameBoard = board.getBoard();
		
		for(int row = 0; row < gameBoard.length; row++)
		{
			boolean availableTileFound = false;
			String firstMove = "";
			for(int column = 0; column < gameBoard[0].length; column++)
			{
				if(availableTileFound && gameBoard[row][column].getTokenOnTile() == null)
				{
					String secondMove = gameBoard[row][column].getTileName();
					possibleMoves.add(new MoveNode(firstMove, secondMove));
					firstMove = secondMove;
				}
				
				if(gameBoard[row][column].getTokenOnTile() == null)
				{
					availableTileFound = true;
					firstMove = gameBoard[row][column].getTileName();
				}
				else
				{
					availableTileFound = false;
				}
			}
		}
		
		return possibleMoves;
	}
}