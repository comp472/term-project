package Player;

import java.util.ArrayList;

import Board.Board;
import Board.Tile;
import MoveTree.MoveNode;
import Token.*;

public class WhitePlayer extends Player
{
	public WhitePlayer(String name)
	{
		super(name);
	}

	@Override
	public WhiteToken getToken() 
	{
		return new WhiteToken();
	}

	@Override
	public boolean isMoveAllowed(Board board, String tokenOnePosition, String tokenTwoPosition) 
	{
		int rowIndexTokenOne = board.getTileCharToInt().get(tokenOnePosition.charAt(0));
		int rowIndexTokenTwo = board.getTileCharToInt().get(tokenTwoPosition.charAt(0));
		
		int rowIndexDifference = Math.abs(rowIndexTokenOne - rowIndexTokenTwo);
		if(rowIndexDifference != 1 || tokenOnePosition.charAt(1) != tokenTwoPosition.charAt(1))
		{
			System.out.println("You can only place your tokens vertically!");
			return false;
		}
		
		return true;
	}

	@Override
	public String getValidMove() 
	{
		return "vertically";
	}

	@Override
	public ArrayList<MoveNode> getAllPossibleMoves(Board board) 
	{
		ArrayList<MoveNode> possibleMoves = new ArrayList<MoveNode>();
		Tile[][] gameBoard = board.getBoard();
		
		for(int column = 0; column < gameBoard[0].length; column++)
		{
			boolean availableTileFound = false;
			String firstMove = "";
			for(int row = 0; row < gameBoard.length; row++)
			{
				if(availableTileFound && gameBoard[row][column].getTokenOnTile() == null)
				{
					String secondMove = gameBoard[row][column].getTileName();
					possibleMoves.add(new MoveNode(firstMove, secondMove));
					firstMove = secondMove;
				}
				
				if(gameBoard[row][column].getTokenOnTile() == null)
				{
					availableTileFound = true;
					firstMove = gameBoard[row][column].getTileName();
				}
				else
				{
					availableTileFound = false;
				}
			}
		}
		
		return possibleMoves;
	}
}
