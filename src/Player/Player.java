package Player;

import java.util.ArrayList;

import Board.Board;
import Board.MoveManager;
import MoveTree.MoveNode;
import Token.*;

public abstract class Player 
{
	private String name;
	private MoveManager manager;
	
	public Player(String name)
	{
		this.name = name;
		manager = new MoveManager();
	}
	
	public String getName() 
	{
		return name;
	}
	
	public boolean play(Board board, String tokenOnePosition, String tokenTwoPosition)
	{
		return manager.placeTokens(this, board, tokenOnePosition, tokenTwoPosition);
	}

	public abstract Token getToken();
	public abstract boolean isMoveAllowed(Board board, String tokenOnePosition, String tokenTwoPosition);
	public abstract ArrayList<MoveNode> getAllPossibleMoves(Board board);
	public abstract String getValidMove();
}
